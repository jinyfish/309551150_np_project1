#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <vector>
#include <queue>
#include <iostream>
using namespace std;

typedef struct command {
	int argc;
    int index;
	char* argv[256];
}command;
typedef struct pipeTable{
    int read;
    int write;
    int life;
}pipeTable;

typedef struct skippipe{
    int read;
    int write;
    int life;
}skippipe;

int parseInput(char* buf, command *commands);
int parseCommand(char* buf, command *commands);

int main()
{
    setenv("PATH", "bin:.", 1);
    char buf[15000];
    skippipe skiptable[5000];
    int skipIndex=0;
    int lastEnd = 0;
    while(1)
    {
        pipeTable ptable[5000];
            int ptableIndex=0;
        for(int i=0;i<skipIndex;i++){
            if(skiptable[i].life >-1 )
                skiptable[i].life = skiptable[i].life - 1;
        }
        int child_status=0;
        int end_with_number = 0;
        int end_with_mark = 0;
        int end_with_file = 0;
        int hasInput = -1;
        command commands[2000];
        cout <<'%' <<' ';
        fgets(buf,15000,stdin);
        if(strstr(buf,"!")!=NULL)
            end_with_mark = 1;
        if(strcmp(buf,"exit\n")==0)
            break;
        buf[strlen(buf)-1]='\0';

        int cmdCnt=parseInput(buf,commands);
        if(cmdCnt==0)
            continue;
        if(isdigit(commands[cmdCnt-1].argv[0][0])  ){
            end_with_number = 1;
            cmdCnt--; 
        }
        char * fileName=NULL;
        struct command *last = &commands[cmdCnt - 1];
        if (last->argc > 2) {
            if (last->argv[last->argc - 2][0] == '>') { 
                end_with_file = 1;
                fileName = last->argv[last->argc - 1];
                last->argv[last->argc - 1]  = NULL;
                last->argv[last->argc - 2] = NULL;
                last->argc -= 2;
            }   
        }
        for(int i=0;i<cmdCnt;i++){
            int p1[2];
            int pipeStatus = pipe(p1);
            if (strcmp(commands[0].argv[0], "setenv") == 0) {
		        if (commands[0].argc == 3) {
			        setenv(commands[0].argv[1], commands[0].argv[2], 1);
		        }
	        }
            else if(strcmp(commands[0].argv[0], "printenv") == 0) {
		        char* env;
		        if (commands[0].argc == 2) {
			        env = getenv(commands[0].argv[1]);
		        }   
	        }

            ptable[i].read = p1[0];
            ptable[i].write = p1[1];
            ptable[i].life = 1;
            ptableIndex+=1;
            
            if(end_with_number == 1 && i==cmdCnt-1 ){
                int need_merge = 0;
                for(int j=0;j<skipIndex;j++){
                    if(skiptable[j].life == atoi(commands[cmdCnt].argv[0]) && skiptable[j].life>0){
                        skiptable[skipIndex].read = skiptable[j].read;
                        skiptable[skipIndex].write = skiptable[j].write;
                        skiptable[skipIndex].life = skiptable[j].life;
                        need_merge = 1;
                        ptable[i].life = skiptable[j].life;
                    }
                
                }
                if(need_merge == 0 ){
                    skiptable[skipIndex].read = p1[0];
                    skiptable[skipIndex].write = p1[1];
                    skiptable[skipIndex].life = atoi(commands[cmdCnt].argv[0]);
                    ptable[i].life =  atoi(commands[cmdCnt].argv[0]);
                }
                skipIndex = skipIndex + 1;

            }

            if(pipeStatus)
                cout << "error" << endl;
            int pid = fork();
            if(pid==0){
                for(int j=0;j<skipIndex;j++){
                    if(skiptable[j].life==0){
                        hasInput = j;
                        break;
                    }
                }
                if(i==0 && cmdCnt==1){
                    if(end_with_file == 1){
                        int newfd;
                        newfd = open(fileName , O_CREAT | O_TRUNC | O_WRONLY , 0777);
                        end_with_number = 0;
                        dup2(newfd,1);
                    }
                    if(end_with_number == 1){
                        dup2(skiptable[skipIndex-1].write,1);
                    }
                    if(end_with_mark == 1){
                        dup2(skiptable[skipIndex-1].write,2);
                    }
                    if(hasInput != -1)
                        dup2(skiptable[hasInput].read,0);
                }
                else if(i==0 && cmdCnt>1){
                    dup2(ptable[i].write,1);
                    if(hasInput != -1)
                        dup2(skiptable[hasInput].read,0);
                    //else 
                        //close(ptable[i].read);
                }
                else if (i>0 && i< cmdCnt-1){
                    dup2(ptable[i-1].read,0);
                    dup2(ptable[i].write,1);
                    
                }
                else{
                    dup2(ptable[i-1].read,0);
                    if(end_with_number == 1)
                        dup2(skiptable[skipIndex-1].write,1);
                    if(end_with_mark == 1){
                        dup2(skiptable[skipIndex-1].write,2);
                    }
                    if(end_with_file == 1){
                        int newfd;
                        newfd = open(fileName , O_CREAT | O_TRUNC | O_WRONLY , 0777);
                        dup2(newfd,1);
                    }
                }   
                int child_status = execvp(commands[i].argv[0],commands[i].argv) + 1;
                if(child_status == 0){
                    cout << "Unknown command: [" << commands[i].argv[0] << "]" << endl;
                    exit(EXIT_FAILURE);
                    break;
                }
            }
            else { 
                if(end_with_number==0 || i!=cmdCnt-1)
                    close(ptable[i].write);
                for(int j=0;j<skipIndex;j++){
                    if(skiptable[j].life==1){
                        close(skiptable[j].write);
                    }
                }
                if(ptable[i-1].life==1){
                        close(ptable[i-1].read);
                }
                if(cmdCnt==1)
                     if(ptable[i].life==1 && end_with_number ==0){
                            close(ptable[i].read);
                            close(ptable[i].write);
                     }      
                wait(&pid);

            }
        }
        lastEnd = end_with_number;
        if(child_status==1)
            break;
    }
    return 0;
}
int writeFile(char * fileName){
    if(fileName == NULL)
        return 0;
    FILE *fp;
    fp = fopen(fileName, "w");
    
}
int parseInput(char* buf, command *commands){
        int cmdCnt=0;
        const char delim[3] = "|!";
        char *token;
        char* saveptr = NULL;
        token = strtok_r(buf, delim,&saveptr);
        if(token == NULL){
            printf("No Input\n");
            return 0;
        }
        commands[0].argc = parseCommand(token,&commands[0]);
        commands[0].index = 0;


        cmdCnt++;
        while( token != NULL ) 
        {
            if(token!=NULL){
                token = strtok_r(NULL, delim,&saveptr);
                commands[cmdCnt].argc = parseCommand(token,&commands[cmdCnt]);
                commands[cmdCnt].index = cmdCnt;
                cmdCnt++;
            }
            commands[cmdCnt].argc = 0;
            commands[cmdCnt].argv[0] = strdup("\0");
            if(token==NULL)
                break;
        }
        return cmdCnt-1;
}

int parseCommand(char* buf, command *commands){
    if(buf == NULL)
        return 0;
    char* substr = NULL;
	char* saveptr = NULL;
	char* dupstr = strdup(buf);
    const char delim[2] = " ";

    substr = strtok_r(dupstr, delim,&saveptr);
	commands->argv[0] = substr;

	if (substr == NULL) {
		return -1;
	}
	int index = 1;
	while (substr) {
		substr = strtok_r(NULL, delim,&saveptr);
		commands->argv[index] = substr;
		index++;
	}
	return index - 1;
}